#include "commandline.h"
#include <QDebug>
#include <QDir>
#include <QProcess>
#include <QShortcut>

Command::~Command(){}
void OpenCommand::execute(QStringList list){
    getCl()->getCurrentEditorPane()->setFile(list[0]);
}

void PwdCommand::execute(QStringList){
    getCl()->setStatusBar(QDir::currentPath());
}

void SaveCommand::execute(QStringList list){
    getCl()->getCurrentEditorPane()->setFilename(list[0]);
    getCl()->save();
}

void ExecCommand::execute(QStringList list){
	QString program = list.first();
	list.removeFirst();
	QProcess p;
	p.start(program, list, QIODevice::ReadWrite);
	p.waitForFinished();
	getCl()->setStatusBar(p.readAllStandardOutput());
	p.close();
}

void VSplitCommand::execute(QStringList list){
	list.size() > 0 ? getCl()->vsplit(list[0]) : getCl()->vsplit();
}

CommandLine::CommandLine(EditorManager* manager) : QWidget() {

	editorManager = manager;

    layout = new QHBoxLayout;
    setLayout(layout);
    commandLineEdit = new QLineEdit;
    QObject::connect(commandLineEdit, &QLineEdit::returnPressed, this, &CommandLine::returnPressed);

    layout->addWidget(commandLineEdit);

	new QShortcut(QKeySequence(Qt::ALT + Qt::Key_I), this, SLOT(previousCommand()));
	new QShortcut(QKeySequence(Qt::ALT + Qt::Key_K), this, SLOT(nextCommand()));

}

CommandLine::~CommandLine(){
    delete layout;
    delete commandLineEdit;
    for(Command *a : commands){
        delete a;
    }
}

void CommandLine::previousCommand(){
	if(currentHistoryCommand >= commandHistory.size()-1) return;
	currentHistoryCommand++;
	qDebug() << currentHistoryCommand;
	commandLineEdit->setText(commandHistory.at(currentHistoryCommand));
}

void CommandLine::nextCommand(){
	currentHistoryCommand--;
	if(currentHistoryCommand == -1) {
		commandLineEdit->setText("");
		return;
	}
	qDebug()<<currentHistoryCommand;
	commandLineEdit->setText(commandHistory.at(currentHistoryCommand));
}

void CommandLine::returnPressed(){
    QStringList list = commandLineEdit->text().split(" ");
    for(Command *a : commands){
        if(a->getActionStr() == list[0]){
            list.removeAt(0);
            a->execute(list);
			if(commandHistory.size() > historyLimit){
				commandHistory.removeLast();
			}
			commandHistory.insert(0, commandLineEdit->text());
			break;
        }
    }
    commandLineEdit->setText("");
    emit actionExecuted();
}


void CommandLine::setFocus(QString s){
    commandLineEdit->setText(s);
    commandLineEdit->setFocus();
}

void CommandLine::setStatusBar(QString s){
   emit setStatusBarSignal(s);
}

void CommandLine::save(){
	emit saveSignal();
}

void CommandLine::vsplit(QString s){
	emit vsplitSignal(s);
}
