ATE is a text editor I made for my personal use.
The colorscheme is [micro text editor](https://micro-editor.github.io/)'s "[darcula](https://github.com/zyedidia/micro/blob/master/runtime/colorschemes/darcula.micro)".

### Example Usage of ATE 
<img src=screenshot1.jpg>
