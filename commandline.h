#ifndef COMMANDLINE_H
#define COMMANDLINE_H

#include <QWidget>
#include <QLineEdit>
#include <QHBoxLayout>

#include "editormanager.h"

class CommandLine;


class Command {
  private:
    CommandLine *cl;
    QString str;
  public:
    Command(QString s, CommandLine *c) { str = s; cl = c; }
    virtual ~Command();

    QString getActionStr(){return str;}
    CommandLine *getCl(){return cl;}
    virtual void execute(QStringList)=0;
};


class OpenCommand : public Command {
public:
    OpenCommand (QString s, CommandLine *c) : Command(s, c) {}
    void execute(QStringList);
};

class PwdCommand : public Command {
public:
    PwdCommand(QString s, CommandLine *c) : Command(s, c){}
    void execute(QStringList);
};

class SaveCommand : public Command {
public:
    SaveCommand(QString s, CommandLine *c) : Command(s, c){}
    void execute(QStringList);
};

class ExecCommand : public Command {
public:
    ExecCommand(QString s, CommandLine *c) : Command(s, c){}
    void execute(QStringList);
};

class VSplitCommand : public Command {
public:
	VSplitCommand(QString s, CommandLine *c) : Command(s, c){}
	void execute(QStringList);
};


class CommandLine : public QWidget {
    Q_OBJECT

private:
    Command *commands[5] = {new OpenCommand("open", this),
                           new PwdCommand("pwd", this),
                           new SaveCommand("save", this),
						   new ExecCommand("exec", this),
						   new VSplitCommand("vsplit", this)};

	const int historyLimit = 5;
	int currentHistoryCommand = -1;
	QStringList commandHistory;

    EditorManager *editorManager;

    QHBoxLayout *layout;
    QLineEdit *commandLineEdit;
    void returnPressed();

public:
    explicit CommandLine(EditorManager*);
    ~CommandLine();

	EditorPane *getCurrentEditorPane(){return editorManager->getCurrentEditor();}

    void setFocus(QString s = "");
    void setStatusBar(QString);
	void save();
	void vsplit(QString s = "");

public slots:
	void previousCommand();
	void nextCommand();

signals:
    void actionExecuted();
    void setStatusBarSignal(QString);
	void saveSignal();
	void vsplitSignal(QString);

};

#endif // COMMANDLINE_H
