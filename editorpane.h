#ifndef EDITORPANE_H
#define EDITORPANE_H

#include <QWidget>
#include <QVBoxLayout>
#include <QPlainTextEdit>
#include <QTextCharFormat>
#include <QLabel>
#include <QFile>
#include <QSyntaxHighlighter>
#include <QRegularExpression>
#include <QVector>
#include <QDebug>

class EditorPane;

class FormatContainer {
public:
    struct Format{
        QTextCharFormat format = QTextCharFormat();
        QString name = "";
    };

//    Format comment;
//    Format identifier;
//    Format constant;
//    Format string;
//    Format statement;
//    Format symbol;
//    Format preproc;
//    Format type;
//    Format special;
//    Format underlined;
//    Format error;
//    Format todo;

//    Format formats[12] = {comment, identifier, constant, string, statement, symbol, preproc,
//                         type, special, underlined, error, todo};

	Format formats[12];
    FormatContainer(){
        formats[0].name = "comment";
        formats[0].format.setForeground(QColor("#707070"));

        formats[1].name = "identifier";
        formats[1].format.setForeground(QColor("#FFC66D"));

        formats[2].name = "constant";
        formats[2].format.setForeground(QColor("#7A9EC2"));

        formats[3].name = "string";
        formats[3].format.setForeground(QColor("#6A8759"));

        formats[4].name = "statement";
        formats[4].format.setForeground(QColor("#CC8242"));

        formats[5].name = "symbol";
        formats[5].format.setForeground(QColor("#CCCCCC"));

        formats[6].name = "preproc";
        formats[6].format.setForeground(QColor("#CC8242"));

        formats[7].name = "type";
        formats[7].format.setForeground(QColor("#CC8242"));

        formats[8].name = "special";
        formats[8].format.setForeground(QColor("#CC8242"));

        formats[9].name = "underlined";
        formats[9].format.setForeground(QColor("#D33682"));

        formats[10].name = "error";
        formats[10].format.setForeground(QColor("#CB4B16"));
        formats[10].format.setFontWeight(QFont::Bold);

        formats[11].name = "todo";
        formats[11].format.setForeground(QColor("#D33682"));
        formats[11].format.setFontWeight(QFont::Bold);

    }

};

class Highlighter : public QSyntaxHighlighter {

private:
    static FormatContainer formatContainer;
    struct HighlightRule {
        QRegularExpression *exp = new QRegularExpression;
        QTextCharFormat *format;
		~HighlightRule(){
			delete exp;
		}
    };
	struct MultilineRule {
		QRegularExpression *start = new QRegularExpression;
		QRegularExpression *end = new QRegularExpression;
		QTextCharFormat *format;
		~MultilineRule(){
			delete start;
			delete end;
		}
	};

	QVector<MultilineRule*> multilineRules;
    QVector<HighlightRule*> rules;
    QString regExFile;


    //create HighlightRules from the syntax file
    //the syntax file has regex-format pairs
    void parseRegExFormatFile();
	void setRegExFile(QString);

public:
	QString filename;
    Highlighter(QTextDocument *e)
		: QSyntaxHighlighter(e){
		filename = "";
		setCurrentBlockState(-1);
	}
	~Highlighter();
    void highlightBlock(const QString&);

    //this two functions set the regExFile
    //depending on the file type
    int checkFileFormat();
    int checkFileHeader();

	void setFilename(QString);
};



class EditorPane : public QWidget
{
    Q_OBJECT
public:
    explicit EditorPane(QWidget *parent = nullptr);
    ~EditorPane();

private:
    QFile *file;
    QVBoxLayout *layout;
    QPlainTextEdit *editor;

    Highlighter *hl;

public:
    void setFocusToTextEdit();
    void setFile(QString);
	QFile *getFile(){return file;};
    int save();
    void setFilename(QString);
	QString getFilename();
	void checkFileFormat();
	QByteArray getText() {return editor->toPlainText().toUtf8();}

signals:
	void filenameChanged();

};

#endif // EDITORPANE_H
