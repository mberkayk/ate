#include "editorpane.h"
#include <QDebug>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

FormatContainer Highlighter::formatContainer = FormatContainer();

Highlighter::~Highlighter(){
	for(int i = 0; i < rules.size(); i++)
		delete rules[i];
	for(int i = 0; i < multilineRules.size(); i++)
		delete multilineRules[i];
}

void Highlighter::setRegExFile(QString s){
	regExFile = s;
	parseRegExFormatFile();
	rehighlight();
}

int Highlighter::checkFileFormat(){

    QFile types(":/syntax/filetypes.json");
    if(!types.open(QFile::ReadOnly)){
        qDebug() << types.errorString();
    }

	QJsonArray arr = QJsonDocument::fromJson(types.readAll()).array();
	for (int i = 0; i < arr.size(); i++){
		QJsonObject obj = arr[i].toObject();
		QRegularExpression exp(obj.value("extension").toString());
		QRegularExpressionMatchIterator iterate = exp.globalMatch(filename);
		if(iterate.hasNext()){
			setRegExFile(":/syntax/" + obj.value("type").toString() + ".json");
		}
	}


    return -1;
}

void Highlighter::parseRegExFormatFile(){
    QFile file(regExFile);
    if(!file.open(QFile::ReadOnly)){
        qDebug() << file.errorString();
    }

	for(int i = 0; i < rules.size(); i++)
		delete rules[i];
	for(int i = 0; i < multilineRules.size(); i++)
		delete multilineRules[i];
	rules.clear();
	multilineRules.clear();

	QJsonObject rootObj = QJsonDocument::fromJson(file.readAll()).object();

	QJsonObject wordRules = rootObj["word-rules"].toObject();
	for(int i = 0; i < 12; i++){
		QString ruleName = formatContainer.formats[i].name;
		if(wordRules.keys().contains(ruleName)){
			QJsonArray expressions(wordRules[ruleName].toArray());
			for(int j = 0; j < expressions.size(); j++){
				HighlightRule *rule = new HighlightRule;
				delete rule->exp;
				rule->exp = new QRegularExpression(expressions[j].toString());
				rule->format = &(formatContainer.formats[i].format);
				rules.append(rule);
			}
		}

		QJsonObject multilineRulesObj = rootObj["multiline-rules"].toObject();
		for(int i = 0; i < 12; i++){
			QString ruleName = formatContainer.formats[i].name;
			if(multilineRulesObj.keys().contains(ruleName)){
				QJsonArray expressions(multilineRulesObj[ruleName].toArray());
				for(int j = 0; j < expressions.size(); j++){	
					MultilineRule *rule = new MultilineRule;
					delete rule->start;
					delete rule->end;
					rule->start = new QRegularExpression (expressions[j].toObject().value("start").toString());
					rule->end = new QRegularExpression (expressions[j].toObject().value("end").toString());
					rule->format = &(formatContainer.formats[i].format);
					multilineRules.append(rule);
				}
			}
		}

	}

}

void Highlighter::highlightBlock(const QString &text){
	if(currentBlockState() == -1){
		for(int i = 0; i < rules.size(); i++){
			HighlightRule *r = rules[i];
			QRegularExpressionMatchIterator it =
					r->exp->globalMatch(text);
			while(it.hasNext()){
				QRegularExpressionMatch match = it.next();
				setFormat(match.capturedStart(), match.capturedLength(),
						  *(r->format));
			}
		}
	}

	if(currentBlockState() == -1){
		for(int i = 0; i < multilineRules.size(); i++){
			QRegularExpressionMatchIterator it =
					multilineRules[i]->start->globalMatch(text);
			if(it.hasNext()){
				QRegularExpressionMatchIterator endIt =
						multilineRules[i]->end->globalMatch(text);
				//make sure that the end and the start
				//expressions aren't picking up the same
				//character
				if(endIt.hasNext()){
					if(it.peekNext().capturedStart() ==
							endIt.peekNext().capturedStart()){
						endIt.next();
					}
				}
				if(endIt.hasNext()){
					setFormat(it.peekNext().capturedStart(),
							  endIt.peekNext().capturedStart()
							  - it.peekNext().capturedStart()
							  + endIt.peekNext().capturedLength(),
							  *(multilineRules[i]->format));
					setCurrentBlockState(-1);
				}
				else{
					setCurrentBlockState(i);
				}
			}
		}
	}else{ //if the state is string or comment
		QRegularExpressionMatchIterator endIt =
				multilineRules[currentBlockState()]->end->globalMatch(text);
		if(endIt.hasNext()){
			int endIndex = endIt.next().capturedEnd();
			setFormat(0, endIndex,
					  *(multilineRules[currentBlockState()]->format));
			setCurrentBlockState(-1);
		}else{
			setFormat(0, text.length(),
					  *(multilineRules[currentBlockState()]->format));
		}
	}

}

void Highlighter::setFilename(QString s){
	filename = s;
}


EditorPane::EditorPane(QWidget *parent) : QWidget(parent) {

    layout = new QVBoxLayout;
	layout->setMargin(0);
    this->setLayout(layout);

    editor = new QPlainTextEdit;
	editor->setFont(QFont("Hack"));
    editor->setTabStopDistance(20);
    layout->addWidget(editor);

    file = new QFile;

    hl = new Highlighter(editor->document());

}

EditorPane::~EditorPane(){
    delete editor;
    delete file;
    delete hl;
}

void EditorPane::setFocusToTextEdit(){
    editor->setFocus();
}

void EditorPane::setFile(QString s){
	delete file;
    file = new QFile(s);
	hl->setFilename(s);
	if(!file->exists()){
        qDebug() << "The specified file doesn't exist";
    }

    if (file->open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream stream(file);
		editor->setPlainText(stream.readAll());
		hl->checkFileFormat();
		emit filenameChanged();
	}
    file->close();
}

int EditorPane::save(){

}

void EditorPane::setFilename(QString s){
    file->setFileName(s);
	hl->setFilename(s);
	emit filenameChanged();
}

QString EditorPane::getFilename(){
	if(file->fileName() == "") return "unnamed";
	else return file->fileName();
}

void EditorPane::checkFileFormat(){
	hl->checkFileFormat();
}
