#ifndef EDITORMANAGER_H
#define EDITORMANAGER_H

#include <QWidget>
#include <QTabWidget>
#include <QGridLayout>

#include "editorpane.h"

class TabbedPanel : public QTabWidget {
	Q_OBJECT

private:
	EditorPane *currentEditor;

public:
	TabbedPanel();
	EditorPane *getCurrentEditor(){return currentEditor;}

public slots:
	void filenameChanged();
};

class EditorManager : public QWidget {
	Q_OBJECT

private:
	QGridLayout *layout;
	QVector<TabbedPanel*> tabbedPanels;
	TabbedPanel *currentPanel;

public:
	explicit EditorManager(QWidget *parent = nullptr);
	~EditorManager();

	EditorPane *getCurrentEditor(){return currentPanel->getCurrentEditor();}

public slots:
	void vsplit(QString);
};

#endif // EDITORMANAGER_H
