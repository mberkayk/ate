#include "mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
    centralWidget = new QWidget;
    mainLayout = new QVBoxLayout;
	mainLayout->setContentsMargins(0, 0, 0, 0);
    centralWidget->setLayout(mainLayout);
    setCentralWidget(centralWidget);

    editorManager = new EditorManager;
    mainLayout->addWidget(editorManager);

    statusBar = new QLabel;
    mainLayout->addWidget(statusBar);

    commandLine = new CommandLine(editorManager);
    QObject::connect(commandLine, SIGNAL(actionExecuted()), this, SLOT(actionExecuted()));
    QObject::connect(commandLine, SIGNAL(setStatusBarSignal(QString)),
                     this, SLOT(setStatusBar(QString)));
	QObject::connect(commandLine, SIGNAL(saveSignal()), this, SLOT(save()));

	QObject::connect(commandLine, SIGNAL(vsplitSignal(QString)),
					 editorManager, SLOT(vsplit(QString)));

    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_S), this, SLOT(save()));
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_O), this,SLOT(open()));
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_E), this, SLOT(focusActionLine()));

}

MainWindow::~MainWindow() {
    delete editorManager;
    delete statusBar;
}

void MainWindow::setStatusBar(QString s){
    statusBar->setText(s);
}

void MainWindow::save(){
	QFile *file = editorManager->getCurrentEditor()->getFile();
	if(editorManager->getCurrentEditor()->getFilename() == ""){
        addCommandLine("save ");
    }else if(file->open(QIODevice::Text | QIODevice::WriteOnly)){
        file->write(editorManager->getCurrentEditor()->getText());
        setStatusBar("Saved " + file->fileName());
        file->close();

        editorManager->getCurrentEditor()->checkFileFormat();
    }else{
        setStatusBar("Couldn't save to the file");
    }
}

void MainWindow::addCommandLine(QString s){
    commandLine->setVisible(true);
    mainLayout->addWidget(commandLine);
    commandLine->setFocus(s);
}

void MainWindow::open(){
    addCommandLine("open ");
}

void MainWindow::focusActionLine(){
    addCommandLine();
}

void MainWindow::actionExecuted(){
    mainLayout->removeWidget(commandLine);
    commandLine->setVisible(false);
    editorManager->getCurrentEditor()->setFocusToTextEdit();
}
