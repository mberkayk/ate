#include "editormanager.h"

TabbedPanel::TabbedPanel(){
	setTabsClosable(true);

	EditorPane *editor = new EditorPane;
	currentEditor = editor;
	addTab(editor, editor->getFilename());
	QObject::connect(editor, SIGNAL(filenameChanged()), this, SLOT(filenameChanged()));
}

void TabbedPanel::filenameChanged(){
	QString str = getCurrentEditor()->getFilename();
	str = str.right(str.length() - str.lastIndexOf("/") - 1);
	setTabText(currentIndex(), str);
}


EditorManager::EditorManager(QWidget *parent) : QWidget(parent) {

	layout = new QGridLayout;
	layout->setContentsMargins(0, 0, 0, 0);
	layout->setHorizontalSpacing(0);
	layout->setVerticalSpacing(0);
	setLayout(layout);

	currentPanel = new TabbedPanel;
	tabbedPanels.append(currentPanel);
	layout->addWidget(currentPanel, 0, 0);

}

EditorManager::~EditorManager(){
	for(int i = 0; i < tabbedPanels.size(); i++){
		delete tabbedPanels.at(i);
	}
}

void EditorManager::vsplit(QString){
	TabbedPanel *p = new TabbedPanel;
	currentPanel = p;
	tabbedPanels.append(currentPanel);
	layout->addWidget(currentPanel, 0, layout->columnCount());

}
