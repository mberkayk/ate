#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QShortcut>
#include <QVBoxLayout>
#include <QLabel>

#include "editorpane.h"
#include "editormanager.h"
#include "commandline.h"

class MainWindow : public QMainWindow {
    Q_OBJECT

private:
    QWidget *centralWidget;
    QVBoxLayout *mainLayout;

    EditorManager *editorManager;

    QLabel *statusBar;

    CommandLine *commandLine;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void addCommandLine(QString s="");
    void setStatusBar(QString);

    void save();
    void open();

    void focusActionLine();
    void actionExecuted();


};
#endif // MAINWINDOW_H
